

#include "1st_HistogramZeros_InARange_parameters.h"

#include "1st_HistogramZeros_InARange_2.h"

using namespace imago;


FILE *fout;
FILE *fout_Tom_Gene;

int do1st_Histogram_Zeros(
	const Image& image_in,

	const PARAMETERS_1ST_HISTOGRAM_ZEROS *sParameters_1st_HistogramZerosf,

	int &nIntensityWithMinPixelCountAboveThresholdf,

	int &nPixelCountAboveThresholdMinf,

	int &nIntensityLevelForBinWithOccupancyBelowThresholdf)
{
	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

	int Initializing_GRAYSCALE_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		GRAYSCALE_IMAGE *sGRAYSCALE_Imagef);

	int First_HistogramZeros(
		const int nLowThresholdf,
		const float fPercentageOfBinOccupancyThresholdf,

		const GRAYSCALE_IMAGE *sGrayscale_Imagef,

		int &nIntensityWithMinPixelCountAboveThresholdf,

		int &nPixelCountAboveThresholdMinf,
		int &nIntensityLevelForBinWithOccupancyBelowThresholdf);

	///////////////////////////////////////////////////////////////////////////////
	fout = fopen("wMain_1st_HistogramZeros.txt", "w");

	if (fout == NULL)
	{
		printf("\n\n fout == NULL");
		getchar(); exit(1);
	} //if (fout == NULL)

	fout_Tom_Gene = fopen("wOutputForTom_Gene_HistogramZeros.txt", "w");

	if (fout_Tom_Gene == NULL)
	{
		printf("\n\n fout_Tom_Gene == NULL");
		getchar(); exit(1);
	} //if (fout_Tom_Gene == NULL)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int
		nResf,
		i,
		j,

		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,

		nImageWidth,
		nImageHeight;

////////////////////////////////////////////////////////////////////////
	// size of image
		nImageWidth = image_in.width();
		nImageHeight = image_in.height();

		float
			fPercentageOfBinOccupancyThresholdf;

	printf("\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);

	fprintf(fout, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
		fprintf(fout,"\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);

		printf("\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		printf("\n\n Please press any key to exit");
		fflush(fout);
		getchar(); exit(1);

	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
		printf("\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		printf("\n\n Please press any key to exit");
		getchar(); exit(1);

	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	
		fprintf(fout_Tom_Gene, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);

	COLOR_IMAGE sColor_Image; //

	nResf = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	GRAYSCALE_IMAGE sGrayscale_Imagef;

	nResf = Initializing_GRAYSCALE_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		&sGrayscale_Imagef); // GRAYSCALE_IMAGE *sGRAYSCALE_Imagef)

	nSizeOfImage = nImageWidth*nImageHeight;

//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j*nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				printf("\n\n Please press any key to exit");
				getchar(); exit(1);
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

				if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

				if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

			//fprintf(fout, "\n nPixelArr[%d] = %d, i = %d, j = %d", nIndexOfPixel, nPixelArr[nIndexOfPixel], i, j);
		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j*nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
//rescaling
			sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed  / 256;

			sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
			sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue  / 256;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
// no rescaling
				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;

				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //else

// the same as Red
			sGrayscale_Imagef.nGrayScale_Arr[nIndexOfPixelCur] = sColor_Image.nRed_Arr[nIndexOfPixelCur];

			if (sGrayscale_Imagef.nGrayScale_Arr[nIndexOfPixelCur] <= sParameters_1st_HistogramZerosf->nIntensityThresholdForValidPixelsMaxf &&
				sGrayscale_Imagef.nGrayScale_Arr[nIndexOfPixelCur] >= sParameters_1st_HistogramZerosf->nIntensityThresholdForValidPixelsMinf)
			{
				sGrayscale_Imagef.nPixel_ValidOrNotArr[nIndexOfPixelCur] = 0;
			} // if (sGrayscale_Imagef.nGrayScale_Arr[nIndexOfPixelCur] <= sParameters_1st_HistogramZerosf->nIntensityThresholdForValidPixelsMaxf &&
			else
				sGrayscale_Imagef.nPixel_ValidOrNotArr[nIndexOfPixelCur] = 1;

		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

/////////////////////////////////////////////////////////////////////////////////

	nResf = First_HistogramZeros(
		sParameters_1st_HistogramZerosf->nLowThresholdf, //const int nLowThresholdf,
		sParameters_1st_HistogramZerosf->fPercentageOfBinOccupancyThresholdf, //const float fPercentageOfBinOccupancyThresholdf,

		&sGrayscale_Imagef, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,

		nIntensityWithMinPixelCountAboveThresholdf, //int &nIntensityWithMinPixelCountAboveThresholdf,

		nPixelCountAboveThresholdMinf, //int &nPixelCountAboveThresholdMinf,
		nIntensityLevelForBinWithOccupancyBelowThresholdf); // int &nIntensityLevelForBinWithOccupancyBelowThresholdf);

/////////////////////////////////////////////////////////////////////////////////////////////
	delete [] sColor_Image.nRed_Arr;
	delete[] sColor_Image.nGreen_Arr;
	delete[] sColor_Image.nBlue_Arr;

	delete[] sGrayscale_Imagef.nGrayScale_Arr;
	delete[] sGrayscale_Imagef.nPixel_ValidOrNotArr;

//	printf("\n\n The grayscale image has been saved");
	//printf("\n\n Please press any key to exit");	getchar(); 

	return 1;
//////////////////////////////////////////////////////////////////////////
} //int do1st_Histogram_Zeros(...
////////////////////////////////////////////////////////////////////////////////

int First_HistogramZeros(
	const int nLowThresholdf,
	const float fPercentageOfBinOccupancyThresholdf,

	const GRAYSCALE_IMAGE *sGrayscale_Imagef,

	int &nIntensityWithMinPixelCountAboveThresholdf,

	int &nPixelCountAboveThresholdMinf,

	int &nIntensityLevelForBinWithOccupancyBelowThresholdf)
{
	int
		nNumOfValidPixelsTotf = 0,

		nNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat],

		nWidthImagef = sGrayscale_Imagef->nWidth,
		nLenImagef = sGrayscale_Imagef->nLength,

		nIndexOfPixelCurf,
		
		nIntensityMinf = nLarge,
		nIntensityMaxf = -nLarge,

		nNumOfPixelsInBinAboveThresholdMinf,

		nNumOfPixels_IntensityLevelForBinWithOccupancyBelowThresholdf, 
		iIntensityf,

		nIntensityCurf,
		iWidf,
		iLenf;

	float
		fBinPercentagef;

	
	for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{
		nNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;

	} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	printf("\n\n First_HistogramZeros: nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);
	fprintf(fout, "\n\n First_HistogramZeros: nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);

	//////////////////////////////////////////////////////////////////////////////
	for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
	{
		for (iLenf = 0; iLenf <nLenImagef; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLenMax);

			if (nIndexOfPixelCurf >= nImageSizeMax)
			{
				printf("\n\n An error in 'First_HistogramZeros': nIndexOfPixelCurf = %d >= nImageSizeMax = %d", nIntensityCurf, nImageSizeMax);
				fprintf(fout, "\n\n An error in 'First_HistogramZeros':  nIndexOfPixelCurf = %d >= nImageSizeMax = %d", nIntensityCurf, nImageSizeMax);
			
				printf("\n\n Please press any key to exit:");
				fflush(fout); getchar(); exit(1);
			} // if (nIndexOfPixelCurf >= nImageSizeMax)

			if (sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
				continue;

			nIntensityCurf = sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf];

			if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat)
			{
				printf("\n\n An error in 'First_HistogramZeros': the grayscale intensity nIntensityCurf = %d", nIntensityCurf);

				fprintf(fout, "\n\n An error in 'First_HistogramZeros': the grayscale intensity nIntensityCurf = %d", nIntensityCurf);
				printf("\n\n Please press any key to exit:");
				fflush(fout); getchar(); exit(1);
			} //if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat)

			if (nIntensityCurf < nIntensityMinf)
				nIntensityMinf = nIntensityCurf;

			if (nIntensityCurf > nIntensityMaxf)
				nIntensityMaxf = nIntensityCurf;

			nNumOfValidPixelsForIntensitiesArrf[nIntensityCurf] += 1;

			nNumOfValidPixelsTotf += 1;
		} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)
	} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

	//nNumOfPixels_IntensityLevelForBinWithOccupancyBelowThresholdf = (int)((float)(nNumOfValidPixelsTotf)*fPercentageOfBinOccupancyThresholdf);
//switching to decimals
	nNumOfPixels_IntensityLevelForBinWithOccupancyBelowThresholdf = (int)((float)(nNumOfValidPixelsTotf)*fPercentageOfBinOccupancyThresholdf*0.01);

	if (nNumOfValidPixelsTotf <= 1)
	{
		fprintf(fout, "\n\n An error in 'First_HistogramZeros': the total number of valid pixels = %d", nNumOfValidPixelsTotf);
		printf( "\n\n An error in 'First_HistogramZeros': the total number of valid pixels = %d", nNumOfValidPixelsTotf);
		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} // if (nNumOfValidPixelsTotf <= 1)
///////////////////////////////////////////////////////////////////////////////////
	printf("\n\n The Histogram:");

	fprintf(fout_Tom_Gene, "\n\n The_Histogram:");

	for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{
		//fBinPercentagef = (float)(nNumOfValidPixelsForIntensitiesArrf[iIntensityf]) / (float)(nNumOfValidPixelsTotf);
		fBinPercentagef = (float)(nNumOfValidPixelsForIntensitiesArrf[iIntensityf]) / (float)(nNumOfValidPixelsTotf)*100.0;
		printf("\n\n Bin #%d, the count = %d, fBinPercentagef = %E", iIntensityf, nNumOfValidPixelsForIntensitiesArrf[iIntensityf], fBinPercentagef);
		
		fprintf(fout, "\n\n Bin #%d, the count = %d, fBinPercentagef = %E", iIntensityf, nNumOfValidPixelsForIntensitiesArrf[iIntensityf], fBinPercentagef);
		fprintf(fout_Tom_Gene, "\n\n Bin #%d, the count = %d, fBinPercentagef = %E", iIntensityf, nNumOfValidPixelsForIntensitiesArrf[iIntensityf], fBinPercentagef);

	} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

		nIntensityWithMinPixelCountAboveThresholdf = nLarge;

		nPixelCountAboveThresholdMinf = nLarge;

		nIntensityLevelForBinWithOccupancyBelowThresholdf = -1; //invalid initially

	for (iIntensityf = nLowThresholdf; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{
		if (nIntensityLevelForBinWithOccupancyBelowThresholdf == -1)
		{
			if (nNumOfValidPixelsForIntensitiesArrf[iIntensityf] < nNumOfPixels_IntensityLevelForBinWithOccupancyBelowThresholdf)
			{
				nIntensityLevelForBinWithOccupancyBelowThresholdf = iIntensityf;

			} // if (nNumOfValidPixelsForIntensitiesArrf[iIntensityf] < nNumOfPixels_IntensityLevelForBinWithOccupancyBelowThresholdf)

		} //if (nIntensityLevelForBinWithOccupancyBelowThresholdf == -1)

		if (nNumOfValidPixelsForIntensitiesArrf[iIntensityf] < nPixelCountAboveThresholdMinf)
		{
			nPixelCountAboveThresholdMinf = nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

			//if (iIntensityf < nIntensityWithMinPixelCountAboveThresholdf)
			nIntensityWithMinPixelCountAboveThresholdf = iIntensityf;

		} // if (nNumOfValidPixelsForIntensitiesArrf[iIntensityf] < nInPixelCountAboveThresholdMinf)

	} // for (iIntensityf = nLowThresholdf; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	if (nIntensityLevelForBinWithOccupancyBelowThresholdf == -1)
		nIntensityLevelForBinWithOccupancyBelowThresholdf = nIntensityStatMax;

	printf("\n\n First_HistogramZeros: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);
	fprintf(fout, "\n\n First_HistogramZeros: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);

	fprintf(fout_Tom_Gene, "\n\n First_HistogramZeros: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);

	fflush(fout); 

	if (nIntensityLevelForBinWithOccupancyBelowThresholdf <= 0 || nIntensityLevelForBinWithOccupancyBelowThresholdf > nNumOfHistogramBinsStat)
	{
		printf("\n\n An error in 'First_HistogramZeros': nIntensityLevelForBinWithOccupancyBelowThresholdf = %d is wrong", nIntensityLevelForBinWithOccupancyBelowThresholdf);
		fprintf(fout, "\n\n An error in 'First_HistogramZeros': nIntensityLevelForBinWithOccupancyBelowThresholdf = %d is wrong", nIntensityLevelForBinWithOccupancyBelowThresholdf);

		printf("\n\n Please press any key to exit:");
		fflush(fout); getchar(); exit(1);
	} //if (nIntensityLevelForBinWithOccupancyBelowThresholdf <= 0 || ...)

	//printf("\n\n The end of 'First_HistogramZeros': please press any key to continue"); getchar();
	return 1;
} //int First_HistogramZeros(....

///////////////////////////////////////////////////////////////////////////

int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} //int Initializing_Color_To_CurSize(...


int Initializing_GRAYSCALE_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	GRAYSCALE_IMAGE *sGRAYSCALE_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf,
		iWidf,
		iLenf;

	sGRAYSCALE_Imagef->nWidth = nImageWidthf;
	sGRAYSCALE_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
	sGRAYSCALE_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
	sGRAYSCALE_Imagef->nGrayScale_Arr = new int[nImageSizeCurf];

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			sGRAYSCALE_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for all pixels
			sGRAYSCALE_Imagef->nGrayScale_Arr[nIndexOfPixelCurf] = -1;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} //int Initializing_GRAYSCALE_To_CurSize(...

//printf("\n\n Please press any key:"); getchar();