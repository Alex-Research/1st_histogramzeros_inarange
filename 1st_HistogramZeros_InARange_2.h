#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

	#include "image.h"

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

#define nLenMax 6000 
#define nWidMax 6000 

#define nLenMin 50 
#define nWidMin 50

#define nImageSizeMax (nLenMax*nWidMax)

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
//#define pi 3.14159
#define PI 3.1415926535

//////////////////////////////////////////////////////////////////
#define nLenSubimageToPrintMin 900 //342 
#define nLenSubimageToPrintMax 1300 //426

#define nWidSubimageToPrintMin 350 //814 
#define nWidSubimageToPrintMax 550
///////////////////////////////////////////////////////////////////////

#define nIntensityStatMin 10 //85 
#define nIntensityStatMinForEqualization 0 //85 

	#define nIntensityStatForReadMax 65535

#define nIntensityStatMax 255 

//#define nNumOfHistogramBinsStat (nIntensityStatMax) //(nIntensityStatMax - nIntensityStatMin + 1)
#define nNumOfHistogramBinsStat (nIntensityStatMax + 1) //(nIntensityStatMax - nIntensityStatMin + 1)

#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax

#define fHueUndefinedValue (-1.0)

/////////////////////////////////////////////////////////

typedef struct
{
	int nWidth;
	int nLength;

//	int nRed_Arr[nImageSizeMax];
//	int nGreen_Arr[nImageSizeMax];
//	int nBlue_Arr[nImageSizeMax];
	int *nRed_Arr;
	int *nGreen_Arr;
	int *nBlue_Arr;

} COLOR_IMAGE;

typedef struct
{
	int nWidth;
	int nLength;

	int *nPixel_ValidOrNotArr; //1 - valid, 0 - invalid
	int *nGrayScale_Arr;
	
} GRAYSCALE_IMAGE;

typedef struct
{
	int nLowThresholdf; // = 140; //0 //10

	int nIntensityThresholdForValidPixelsMinf;
	int nIntensityThresholdForValidPixelsMaxf;

	float fPercentageOfBinOccupancyThresholdf;
} PARAMETERS_1ST_HISTOGRAM_ZEROS;

