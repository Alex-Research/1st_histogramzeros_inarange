
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include "1st_HistogramZeros_InARange_function.cpp"

int main()
{
	int do1st_Histogram_Zeros(

		const Image& image_in,

		const PARAMETERS_1ST_HISTOGRAM_ZEROS *sParameters_1st_HistogramZerosf,
		int &nIntensityWithMinPixelCountAboveThresholdf,

		int &nPixelCountAboveThresholdMinf,
		
		int &nIntensityLevelForBinWithOccupancyBelowThresholdf);

	int
		nIntensityLevelForBinWithOccupancyBelowThresholdf,
		nIntensityWithMinPixelCountAboveThresholdf,
		nPixelCountAboveThresholdMinf,
		nRes;

		//const char inputFileName[] = "IM-0001-0738_RF_NoLabels.png";
	PARAMETERS_1ST_HISTOGRAM_ZEROS sPARAMETERS_1ST_HISTOGRAM_ZEROS;
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	sPARAMETERS_1ST_HISTOGRAM_ZEROS.nLowThresholdf = nLowThreshold;
	sPARAMETERS_1ST_HISTOGRAM_ZEROS.nIntensityThresholdForValidPixelsMinf = nIntensityThresholdForValidPixelsMin;
	sPARAMETERS_1ST_HISTOGRAM_ZEROS.nIntensityThresholdForValidPixelsMaxf = nIntensityThresholdForValidPixelsMax;

	sPARAMETERS_1ST_HISTOGRAM_ZEROS.fPercentageOfBinOccupancyThresholdf = fPercentageOfBinOccupancyThreshold;

	Image image_in;
	
	image_in.read("03-023-0-RCC_Neg_c_abX_c_NoLabels.png");

	Image image_out;

	nRes = do1st_Histogram_Zeros(
		//inputFileName, //const char *inputFileNamef,
		image_in, // Image& image_in,

		&sPARAMETERS_1ST_HISTOGRAM_ZEROS, // const PARAMETERS_WEIGHTED_GRAY_3x3 *sParameters_1st_HistogramZerosf); // const float fWeight_Bluef)

		nIntensityWithMinPixelCountAboveThresholdf,
		nPixelCountAboveThresholdMinf,

		nIntensityLevelForBinWithOccupancyBelowThresholdf); //int &nIntensityLevelForBinWithOccupancyBelowThresholdf);
	
	if (nIntensityLevelForBinWithOccupancyBelowThresholdf < nLowThreshold || nIntensityLevelForBinWithOccupancyBelowThresholdf > nIntensityStatMax)
	{
		printf( "\n\n An error: intensity level %d is wrong ", nIntensityLevelForBinWithOccupancyBelowThresholdf);
		fprintf(fout, "\n\n An error: intensity level %d is wrong ", nIntensityLevelForBinWithOccupancyBelowThresholdf);
		printf("\n\n Please press any key to exit");		fflush(fout);
		getchar(); exit(1);
	}// if (nIntensityLevelForBinWithOccupancyBelowThresholdf < nLowThreshold || nIntensityLevelForBinWithOccupancyBelowThresholdf > nIntensityStatMax)

	printf("\n\n //////////////////////////////////////////////////////////////");
	printf("\n\n The intensity level corresponding to 'fPercentageOfBinOccupancyThreshold' is %d ", nIntensityLevelForBinWithOccupancyBelowThresholdf);

	fprintf(fout,"\n\n //////////////////////////////////////////////////////////////");
	fprintf(fout, "\n\n The intensity level corresponding to 'fPercentageOfBinOccupancyThreshold' is %d ", nIntensityLevelForBinWithOccupancyBelowThresholdf);
	
	fprintf(fout_Tom_Gene,"\n\n //////////////////////////////////////////////////////////////");
	fprintf(fout_Tom_Gene, "\n\n The intensity level corresponding to 'fPercentageOfBinOccupancyThreshold' is %d ", nIntensityLevelForBinWithOccupancyBelowThresholdf);

	//printf("\n\n nIntensityWithMinPixelCountAboveThresholdf = %d, the distance to the threshold = %d", 
		//nIntensityWithMinPixelCountAboveThresholdf, nIntensityWithMinPixelCountAboveThresholdf - nLowThreshold);

	fprintf(fout,"\n\n nIntensityWithMinPixelCountAboveThresholdf = %d, the distance to the threshold = %d",
		nIntensityWithMinPixelCountAboveThresholdf, nIntensityWithMinPixelCountAboveThresholdf - nLowThreshold);

	if (nPixelCountAboveThresholdMinf > 0)
	{
		printf("\n\n There are no bins below the threshold with zero histogram count");
		printf("\n\n The minimal number of pixels in a bin is %d", nPixelCountAboveThresholdMinf);
		printf("\n\n This number corresponds to the bin with the minimum intensity above the threshold.");

		printf("\n\n The minimum intensity level is %d", nIntensityLevelForBinWithOccupancyBelowThresholdf);

		fprintf(fout,"\n\n There are no bins below the threshold with zero histogram count");
		fprintf(fout,"\n\n The minimal number of pixels in a bin is %d", nPixelCountAboveThresholdMinf);
		fprintf(fout,"\n\n This number corresponds to the bin with the minimum intensity above the threshold.");

		
			fprintf(fout_Tom_Gene, "\n\n There are no bins below the threshold with zero histogram count");
		fprintf(fout_Tom_Gene, "\n\n The minimal number of pixels in a bin is %d", nPixelCountAboveThresholdMinf);
		fprintf(fout_Tom_Gene, "\n\n This number corresponds to the bin with the minimum intensity above the threshold.");

	}//if (nPixelCountAboveThresholdMinf > 0)
	else 	if (nPixelCountAboveThresholdMinf == 0)
	{
		printf("\n\n There are bins below the threshold with zero histogram count");
		fprintf(fout, "\n\n There are bins below the threshold with zero histogram count");
		fprintf(fout_Tom_Gene, "\n\n There are bins below the threshold with zero histogram count");

	}//if (nPixelCountAboveThresholdMinf > 0)	if (nPixelCountAboveThresholdMinf > 0)
	else if (nPixelCountAboveThresholdMinf < 0)
	{
		printf("\n\n An error: the minimal number of pixels in a bin below the threshold is less than zero: %d", nPixelCountAboveThresholdMinf);
		fprintf(fout,"\n\n An error: the minimal number of pixels in a bin below the threshold is less than zero: %d", nPixelCountAboveThresholdMinf);
		printf("\n\n Please press any key to exit");		fflush(fout);
		getchar(); exit(1);
	}//else if (nPixelCountAboveThresholdMinf < 0)


	fflush(fout_Tom_Gene);
	printf("\n\n Please press any key to exit");
	fflush(fout);  getchar();
	return 1;
	  
} //int main()

//printf("\n\n Please press any key:"); getchar();