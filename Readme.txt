
The input parameters described below are located in the file "1st_HistogramZeros_InARange_parameters.h".

1. "nLowThreshold" serves as the low intensity boundary for the range of interest; the upper boundary is 255.
  Currently, "nLowThreshold" equals 190.

2. The parameters "nIntensityThresholdForValidPixelsMin" and "nIntensityThresholdForValidPixelsMax" determine the intensity range for pixels eliminated from the histogram calculation.
Now, they are set up to zeros (the background intensity). 

3. "fPercentageOfBinOccupancyThreshold" (currently, 0.01) specifies the histogram bin occupancy threshold as a percentage of the totals number breast pixels.

//////////////////////////////////////////////////////////////////////////////

The results are placed into the file "wOutputForTom_Gene_HistogramZeros.txt". The file contains a detailed, bin-by-bin information on each histogram bin occupancy with the corresponding percentages relative to the total number of breast pixels. 
The output intensity level is also produced; this is the lowest intensity at which the histogram bin occupancy falls below the specified threshold "fPercentageOfBinOccupancyThreshold". 
If all bin occupancy levels are above the specified threshold, the maximum intensity level of 255 is produced as the outcome.